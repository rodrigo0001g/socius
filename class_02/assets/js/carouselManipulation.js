function setCarrouselMovies(movies){
  let active = "active";
  movies.forEach(movie => {
    $("#carousel_container").append(`<div class='carousel-item ${active}'><div class='carousel-item-personalized'><center><p><h1>${movie.title}</h1></p><p><h2>Genero: ${movie.genre}</h2></p><p><h3>Año: ${movie.year}</h3></p></center></div></div>`);
    active = "";
  })
}
function setGridMovies(movies){
  let counter = 0;
  let first = true;
  let elements = "";
  movies.forEach(movie => {
    if(counter % 4 == 0){
      if(!first){
        elements += "</div>";
      }
      elements += "<div class='row'>";
    }
    elements += `<div class='col-3'><div class='card-movie'><center><p><h1>${movie.title}</h1></p><p><h2>Genero: ${movie.genre}</h2></p><p><h3>Año: ${movie.year}</h3></p></center></div></div>`;
    active = "";
    first= false;
    counter++;
  });
  $("#movie-list").html(elements)  
}

function filterMovie(event){
  console.log(event);
}